# Sliding Tile Puzzle Solver

This program uses an A* search algorithm to find the fewest number of moves
that will correctly solve the classic 3x3 sliding tile puzzle.

## Installation

Simply download the source code and run <make> from command line

## Usage

Enter two 9 digit strings of characters, separated by a newline, that will
represent the following indices in a 3x3 grid:

0 1 2

3 4 5

6 7 8

The first string should represent the initial configuration, the second should
represent the goal state you want to solve for.

Sample input:
123745086
123456780

Sample output:
URRD
Total moves: 4

## Credits

Written by Cody Barnson