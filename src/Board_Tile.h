#ifndef BOARD_TILE_H
#define BOARD_TILE_H

#include <vector>
#include <ostream>

class Board_Tile {
  public:
   //Board_Tile(const std::vector<int>& config);   
   Board_Tile(const std::vector<int>& config, Board_Tile* parent, int depth, char m);
   Board_Tile(const Board_Tile& tile);
   ~Board_Tile();
   
   // get methods
   const std::vector<int>& getConfig() const;
   const std::vector<int>& getOptions() const;
   
   int getDepth() const;   
   int getCurrentManhattan();
   int getEvaluationValue();
   
   //bool isGoal() const;   
   void expand(std::vector<Board_Tile*>);
   void printChildren();

   Board_Tile* _parent;
   std::vector<Board_Tile*> _children;
   char _moveTo;
   int _depth;

  private:
   std::vector<int> _config;
   std::vector<int> _options;
   std::vector<char> _moves;

   friend std::ostream& operator<<(std::ostream& os, const Board_Tile& t);

   // HELPER FUNCTIONS
   void findOptions();
   void swapTiles(int, int);
   int findIndex(int);
   int zeroTilePosition() const;
   int getValue(int);
   


   
};

#endif
