#include "Sliding_Solver.h"

#include <iostream>

#include "Board_Tile.h"

// constructor
Sliding_Solver::Sliding_Solver(const std::vector<int>& config, 
			       const std::vector<int>& goal) {
  _goal = goal;
  _root = new Board_Tile(config, nullptr, 0, '_');
  _current = _root;
}

// destructor
Sliding_Solver::~Sliding_Solver() {
  delete _root;
  //std::cout << "_root deleted\n";
}

bool Sliding_Solver::isGoal() const {
  std::vector<int> cmp = _current->getConfig();
  for (unsigned int i = 0; i < 9; i++) {
    if (cmp[i] != _goal[i])
      return false;
  }
  return true;
}

void Sliding_Solver::solve() {
  // find our solution using initial as the starting point
  while (!isGoal()) {
      
    _current->expand(_explored.getTiles()); // expand does a new call...
    _explored.addTile(_current->getConfig(), _current->_parent, 
		      _current->_depth, _current->_moveTo);

    for (unsigned int i = 0; i < _current->_children.size(); i++) {
      _active.addTile(_current->_children[i]);
    }
    _current = _active.getMin(); 
    // returns a reference to the min object AND removes the
    // object from its Board_Tile_List
  }
  
  // recursively print out the solution
  print(_current);
  std::cout << "\nTotal moves: " << _current->_depth << "\n";
}

void Sliding_Solver::print(Board_Tile* t) {
  if (t->_parent != nullptr)
    print(t->_parent);
  std::cout << *t;
}


