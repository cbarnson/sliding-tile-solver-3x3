#include "Board_Tile.h"
#include <iostream>

// some prototypes
int findRow(int);
int findCol(int);
bool isMatch(const std::vector<int>&, const std::vector<int>&);
bool isRepeated(std::vector<Board_Tile*>, std::vector<int>);
void convertIndex(int, int&, int&);


// CONSTRUCTOR
Board_Tile::Board_Tile(const std::vector<int>& config, Board_Tile* parent, 
		       int depth, char m) {
   _config = config;
   _parent = parent;
   _depth = depth;
   _moveTo = m;
   findOptions();
}

// COPY CONSTRUCTOR
Board_Tile::Board_Tile(const Board_Tile& tile_) {
   _config = tile_.getConfig();
   _parent = tile_._parent;
   _depth = tile_._depth;
   findOptions();
}

// DESTRUCTOR
Board_Tile::~Board_Tile() {
   for (unsigned int i = 0; i < _children.size(); i++) {
      delete _children[i];
   }
   _children.clear();
}


// getConfig()
// returns the configuration of the board
const std::vector<int>& Board_Tile::getConfig() const {
   return this->_config;
}

// getOptions()
// returns a vector containing the possible indices with which the 0 can swap 
// with
const std::vector<int>& Board_Tile::getOptions() const {
   return this->_options;
}

// getDepth()
// returns the current depth of the board state from the initial starting state
int Board_Tile::getDepth() const {
   return this->_depth;
}

// returns the manhattan distance plus the depth to represent the cost of this 
// objects configuration
int Board_Tile::getEvaluationValue() {
   return this->getCurrentManhattan() + this->getDepth();
}

// expand(...)
// dynamically allocates for this objects children vector, which has Board_Tile*
// type for each option, interpreted from the options vector, swap valid 
// indices, and check to see if the object matches one already in our history,
// if it is repeated, do not create the object
// else create the object
// then swap the indices back and repeat for each option
void Board_Tile::expand(std::vector<Board_Tile*> history) {
   
   for (unsigned int i = 0; i < _options.size(); i++) {      
      int a = _options[i];
      int b = zeroTilePosition();
      char c = _moves[i];
      swapTiles(a, b);

      if (!isRepeated(history, _config)) {
	 _children.push_back(new Board_Tile(this->getConfig(), this, 
					    this->getDepth()+1, c)); 
      }
      swapTiles(b, a);
   }
}


// swapTiles(...)
// swaps the values of the two indices provided
void Board_Tile::swapTiles(int index1, int index2) {
   int temp_value = _config[index1];
   _config[index1] = _config[index2];
   _config[index2] = temp_value;   
}

// getCurrentManhattan()
// for indices 0 to 8, calculates their vertical and horizontal distance from 
// their goal positions
int Board_Tile::getCurrentManhattan() {
   int total = 0;
   for (unsigned int i = 0; i < _config.size(); i++) {
      int value = _config[i];
      int finalIndex, iRow, iCol, fRow, fCol;
      
      if (value > 0)
	 finalIndex = value - 1;
      else
	 finalIndex = 8;
      
      iRow = findRow(i);
      iCol = findCol(i);
      fRow = findRow(finalIndex);
      fCol = findCol(finalIndex);

      total += (abs(iRow - fRow) + abs(iCol - fCol));
   }
   return total;
}

// getValue(...)
// returns the value of config at the provided index
int Board_Tile::getValue(int index) {
   return _config[index];
}

// zeroTilePosition()
// returns an int representing the index of the 0 in the objects config
int Board_Tile::zeroTilePosition() const {
   for (unsigned int i = 0; i < _config.size(); i++) {
      if (_config[i] == 0)
	 return i;
   }
   return -1; // error
}


// returns the index of the target
int Board_Tile::findIndex(int target) {
   for (unsigned int i = 0; i < _config.size(); i++) {
      if (_config[i] == target)
	 return i;
   }
   std::cerr << "error in findVal, target not found\n";
   return -1;
}





// takes some position index, returns the row in the 3x3 grid
int findRow(int index) {
   return index / 3;
}

// takes some position index, returns the column in the 3x3 grid
int findCol(int index) {
   return index % 3;
}

bool isMatch(const std::vector<int>& a, const std::vector<int>& b) {
   for (int i = 0; i < 9; i++) {
      if (a[i] != b[i])
	 return false;
   }
   return true;
}

bool isRepeated(std::vector<Board_Tile*> history, std::vector<int> checkConfig)
{
   if (!history.empty()) {
      for (unsigned int i = 0; i < history.size(); i++) {
	 if (isMatch(history[i]->getConfig(), checkConfig))
	    return true; // is a repeated state
      }
   }
   return false; // checkState does not appear in history
}

// takes index, modifies parameters row and col to reflect its equivalent 
// position
// NOTE: row and col each start from 0
void convertIndex(int index, int& row, int& col) {
   row = index / 3;
   col = index % 3;
}

// has to be a better way...
void Board_Tile::findOptions() {
   switch (zeroTilePosition()) {
      case 0:
	 _options.push_back(1); _moves.push_back('R');
	 _options.push_back(3); _moves.push_back('D');
	 break;
      case 1:
	 _options.push_back(0); _moves.push_back('L');
	 _options.push_back(2); _moves.push_back('R');
	 _options.push_back(4); _moves.push_back('D');
	 break;  
      case 2:
	 _options.push_back(1); _moves.push_back('L');
	 _options.push_back(5); _moves.push_back('D');
	 break;
      case 3:
	 _options.push_back(0); _moves.push_back('U');
	 _options.push_back(4); _moves.push_back('R');
	 _options.push_back(6); _moves.push_back('D');
	 break;
      case 4:
	 _options.push_back(1); _moves.push_back('U');
	 _options.push_back(3); _moves.push_back('L');
	 _options.push_back(5); _moves.push_back('R');
	 _options.push_back(7); _moves.push_back('D');
	 break;
      case 5:
	 _options.push_back(2); _moves.push_back('U');
	 _options.push_back(4); _moves.push_back('L');
	 _options.push_back(8); _moves.push_back('D');
	 break;
      case 6:
	 _options.push_back(3); _moves.push_back('U');
	 _options.push_back(7); _moves.push_back('R');
	 break;
      case 7:
	 _options.push_back(4); _moves.push_back('U');
	 _options.push_back(6); _moves.push_back('L');
	 _options.push_back(8); _moves.push_back('R');
	 break;
      case 8:
	 _options.push_back(5); _moves.push_back('U');
	 _options.push_back(7); _moves.push_back('L');
	 break;	 
   }
}


std::ostream& operator<<(std::ostream& os, const Board_Tile& t) {

   if (t._moveTo == '_')
      return os;
   return os << t._moveTo;

   /* // if you wanted to print out the board for error checking
  for (int i = 0; i < 3; i++) 
    os << t._config[i] << " ";   
  os << "\n";
  for (int i = 3; i < 6; i++) 
    os << t._config[i] << " ";   
  os << "\n";
  for (int i = 6; i < 9; i++) 
    os << t._config[i] << " ";   
  return os << "\n\n";
  */
}


