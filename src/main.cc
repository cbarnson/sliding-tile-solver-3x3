#include <vector>
#include <fstream>
#include <iostream>
#include "Board_Tile.h"
#include "Sliding_Solver.h"

int main() {

   std::ifstream file;
   file.open("input");
   if (file.is_open()) {

      std::string initial_;
      std::string goal_;
      
      std::getline(file, initial_);
      std::getline(file, goal_);
      
      std::vector<int> initial(initial_.size());
      for(unsigned int i = 0; i < initial_.size(); ++i) {
	 initial[i] = initial_[i] - '0';
      }
      std::vector<int> goal(goal_.size());
      for(unsigned int i = 0; i < goal_.size(); ++i) {
	 goal[i] = goal_[i] - '0';
      }

      Sliding_Solver puzzle(initial, goal);
      puzzle.solve();
      file.close();
   }
   else {
      std::cout << "file could not open\n";
   }

        
   return 0;
}

