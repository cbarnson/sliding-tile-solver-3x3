#ifndef SLIDING_SOLVER_H
#define SLIDING_SOLVER_H

#include <vector>

#include "Board_Tile.h"
#include "Board_Tile_List.h"

class Sliding_Solver {
  public:
   Sliding_Solver(const std::vector<int>& config, 
		  const std::vector<int>& goal);
   ~Sliding_Solver();
   void solve();
   void print(Board_Tile*);
   bool isGoal() const;
   
  private:
   std::vector<int> _goal;
   Board_Tile* _root;
   Board_Tile* _current;
   Board_Tile_List _explored;
   Board_Tile_List _active;
};


#endif
