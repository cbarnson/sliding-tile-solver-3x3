#ifndef BOARD_TILE_LIST_H
#define BOARD_TILE_LIST_H

#include "Board_Tile.h"

#include <vector>
#include <iostream>

// wrapper for vector of Board_Tile pointers
class Board_Tile_List {

  public:
   // destructor
   ~Board_Tile_List() {
      //int counter = 0;
      for (unsigned int i = 0; i < _tiles.size(); i++) {
	 delete _tiles[i]; //counter++;
      }
      //std::cout << "deleted " << counter << " objects\n";
      //std::cout << "deleted container\n";
   }
   
   Board_Tile* addTile(const std::vector<int>& config, Board_Tile* parent, 
		       int depth, char c) {
      Board_Tile* t = new Board_Tile(config, parent, depth, c);
      _tiles.push_back(t);
      return t;
   }

   Board_Tile* addTile(Board_Tile* tile) {
      Board_Tile* t = new Board_Tile(tile->getConfig(), tile->_parent, 
				     tile->_depth, tile->_moveTo);
      _tiles.push_back(t);
      return t;
   }

   Board_Tile* getMin() {
      auto min = _tiles.begin();
      for (std::vector<Board_Tile*>::iterator it = _tiles.begin(); 
	   it != _tiles.end(); ++it) {
	 if ((*min)->getEvaluationValue() > (*it)->getEvaluationValue()) {
	    min = it;
	 }
      }
      Board_Tile* tmp = *min;
      _tiles.erase(min);
      return tmp;
   }

   // get method
   std::vector<Board_Tile*> getTiles() const {
      return _tiles;
   }

  private:
   std::vector<Board_Tile*> _tiles;

   
};

#endif
